package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class SudokuGrid {
    SudokuSolver sv;
    ArrayList<TextField> texts = new ArrayList<>();
    GridPane table;

    public SudokuGrid(SudokuSolver sv,GridPane table){
        this.sv = sv;
        this.table = table;
    }
    public SudokuGrid(){
        this.sv = new SudokuSolver();
        this.table = new GridPane();
    }

    public void save(File file) {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileOutputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                pw.print(this.sv.grid[i][j]);
                pw.print(" ");
            }
        }
        pw.close();
    }

    public void open(File file) {
        System.out.println("Open");
        for (TextField text : this.texts) {
            text.setText("");
            text.setEditable(false);
            text.setStyle("-fx-control-inner-background: WHITE");
        }
        int[] buffor = new int[81];
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int i = 0;
        while(scanner.hasNextInt()){
            buffor[i++] = scanner.nextInt();
        }
        parse(buffor);
        przypisz(this.sv.grid,this.sv.solvedGrid);
        //this.sv.solvedGrid = this.sv.grid;
        if(this.sv.solveOpened(new SudokuSolver.Cell(0,0))){
            this.sv.saveHints();
            display();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Bad filetype");
            alert.setHeaderText("Your file cannot to be resolved");
            alert.setContentText("Please try again");
            alert.showAndWait();
        }
    }

    public void display(){
        clear();
        for(int i = 0; i<9;i++){
            for(int j= 0; j<9;j++){
                TextField textField = new TextField("");
                if(!(this.sv.grid[i][j] == 0)){
                    textField.setText(String.valueOf(this.sv.grid[i][j]));
                    textField.setDisable(true);
                    textField.setStyle("-fx-control-inner-background: WHITE");
                }
                else{
                    textField.setStyle("-fx-control-inner-background: YELLOW");
                }
                this.texts.add(textField);
                table.add(textField,j,i);
            }
        }
    }

    public void parse(int[] buffor){
        if(buffor.length == 81){
            int c = 0;
            for(int i = 0; i<9 ; i++){
                for(int j = 0; j<9 ; j++) {
                        this.sv.grid[i][j] = buffor[c];
                        c++;
                    }
                }
            }
            else{
            System.out.println("Niepoprawna ilosc znakow w tablicy");
        }
    }

    public void binder(){
        for(TextField text : this.texts){
            text.textProperty().addListener((observable, oldValue, newValue) -> {
                System.out.println("Wartość " + newValue + " została wpisana w  " + "x:" + GridPane.getColumnIndex(text) + " y:" + GridPane.getRowIndex(text));
                this.sv.grid[GridPane.getRowIndex(text)][GridPane.getColumnIndex(text)] = Integer.valueOf(newValue);
            });
        }
    }

    public void clear(){
        for(TextField text : this.texts){
            table.getChildren().remove(text);
        }
    }
    public void przypisz(int[][] f, int[][] s){
        for(int i = 0; i<9 ; i++){
            for(int j = 0; j<9 ; j++) {
                s[i][j] = f[i][j];
            }
        }
    }
    public boolean checkSolution(){
        for(int i = 0; i<9 ; i++) {
            for (int j = 0; j < 9; j++) {
                if (!(this.sv.grid[i][j] == this.sv.solvedGrid[i][j])) {
                    Alert notSolvedAlert = new Alert(Alert.AlertType.CONFIRMATION);
                    notSolvedAlert.setTitle("Wrong Solution!");
                    notSolvedAlert.setHeaderText("Your solved grid is not correct");
                    notSolvedAlert.setContentText("Try again, my friend");
                    notSolvedAlert.showAndWait();
                    return false;
                }
            }
        }
        Alert solvedAlert = new Alert(Alert.AlertType.INFORMATION);
        solvedAlert.setTitle("Good Solution!");
        solvedAlert.setHeaderText("Congrats");
        solvedAlert.setContentText("Nice!");
        solvedAlert.showAndWait();
        return true;
    }
}
