package sample;


public class SudokuSolver {

    // Wymiar
    int N = 9;

    // zmienna
    int grid[][] = new int[9][9];
    int solvedGrid[][] = new int[9][9];
    int hints[][] = new int[9][9];

    public SudokuSolver(int[][] grid){
        this.grid = grid;
    }
    public SudokuSolver(){
        for(int i = 0; i<9;i++) {
            for (int j = 0; j < 9; j++) {
                this.grid[i][j] = 0;
            }
        }
    }

    static class Cell {

        int row, col;

        public Cell(int row, int col) {
            super();
            this.row = row;
            this.col = col;
        }

        @Override
        public String toString() {
            return "Cell [row=" + row + ", col=" + col + "]";
        }
    };



     boolean isValid(Cell cell, int value) {

        if (grid[cell.row][cell.col] != 0) {
            System.out.println("Pole ma juz wartosc rozna od zera - czyli nie jest puste");
        }


        for (int c = 0; c < 9; c++) {
            if (grid[cell.row][c] == value || grid[c][cell.col] == value)
                return false;
        }

        // tak jak rozmawialismy na zajeciach, wyznaczanie kwadratu, a wlasciwie jego wierzcholkow (wspolrzednych).
        int x1 = 3 * (cell.row / 3);
        int y1 = 3 * (cell.col / 3);
        //stworzenie kolejnego wierzchilka
        int x2 = x1 + 2;
        int y2 = y1 + 2;

        //iteracja kwadratu
        for (int x = x1; x <= x2; x++) {
            for (int y = y1; y <= y2; y++) {
                if (grid[x][y] == value) {
                    return false;
                }
            }
        }
        // jezeli liczby nie ma w pionie, poziomie i boxie, return true
        return true;
    }

    // funckja bierze nastepna komorke
     Cell getNextCell(Cell cur) {

        int row = cur.row;
        int col = cur.col;

        // next cell => col++ - najpierw idziemy od lewej do prawej, ponizej warunek ktory schodzi pietro nizej i zaczyna od lewej
        col++;

        // if col > 8, to ustaw columne na zero - czyli od lewej i zejdz pietro nizej czyli row++
        // osiagniecie calego row
        if (col > 8) {
            // idz pietro nizej
            col = 0;
            row++;
        }

        // przejechalismy cala tablice 2d wiec zwroc nulla
        if (row > 8) {
            return null;
        }// koniec

        Cell next = new Cell(row, col);
        return next;
        //ustal nowa komorke i ja zwroc
    }


    // Zwraca prawde jezeli sudoku jest rozwiazane
     boolean solve(Cell cur) {

        // tak jak wyzyej, jesli null to rozwiazane
        if (cur == null) {
            return true;
        }

        // jesli grid[current] ma cos innego niz zero (czyli jest wypelniona) to nic nie mam tu do roboty
        if (grid[cur.row][cur.col] != 0) {
            //Kwintesencja algorytmu, jezeli jest wypelniona to lec po nowa komorke
            return solve(getNextCell(cur));
        }


        // jesli grid[current] nie ma nic to sprawdz czy w komore current mozna zapakowac i
        for (int i = 1; i <= 9; i++) {
            // sprawdz czy jest ok, jesli tak to nizej wypelnij to wartoscia z i
            boolean valid = isValid(cur, i);

            if (!valid) // i not valid for this cell, try other values
                continue;

            // przypisanie
            grid[cur.row][cur.col] = i;

            //---------------------------------------------------------------tutaj tescik jak fajnie wypelnia
            for (int row = 0; row < N; row++) {
                for (int col = 0; col < N; col++)
                    System.out.print(grid[row][col]+" ");
                System.out.println();
            }
            System.out.println("\n\n\n---------------------------------------------------------------\n\n\n");
            //---------------------------------------------------------------


            // lec z nastepna komorka
            boolean solved = solve(getNextCell(cur));
            // jesli jest wypelnione to true
            if (solved)
                return true;
            else // jesli nie to wpisz zero i sie cofamy
                grid[cur.row][cur.col] = 0;
            //---------------------------------------------------------------tescik
            for (int row = 0; row < N; row++) {
                for (int col = 0; col < N; col++)
                    System.out.print(grid[row][col]+" ");
                System.out.println();
            }
            System.out.println("\n\n\n---------------------------------------------------------------\n\n\n");
            //---------------------------------------------------------------
            // continue z innymi wartosciami
        }


        // Jezeli jest cos nie poprawne to zwroc false
         System.out.println("nie mozna");
        return false;
    }


    public void makeHoles(int holesToMake)
    {
		/* Dane wg swiatowej instytucji Sudoku

			Easy: 32+ clues (49 or fewer holes)
			Medium: 27-31 clues (50-54 holes)
			Hard: 26 or fewer clues (54+ holes)
			This is human difficulty, not algorighmically (though there is some correlation)

		*/
        double remainingSquares = 81;
        double remainingHoles = (double)holesToMake;


        for(int i=0;i<9;i++)
            for(int j=0;j<9;j++)
            {
                double holeChance = remainingHoles/remainingSquares;
                if(Math.random() <= holeChance)
                {
                    this.grid[i][j] = 0;
                    remainingHoles--;
                }
                remainingSquares--;
            }
    }


     public void generate(int startWith,int dificulty){
         int[][] newSudoku = new int[9][9];
         for(int i = 0; i<9;i++){
            for(int j= 0; j<9;j++){
                newSudoku[i][j] = 0;
            }
         }
         newSudoku[0][0] = startWith;
         this.grid = newSudoku;
         this.solve(new Cell(0,0));
         for(int i = 0; i<9;i++){
             for(int j= 0; j<9;j++){
                 this.solvedGrid[i][j] = this.grid[i][j]; //
             }
         }
         this.makeHoles(dificulty);
         this.saveHints();
    }


     void printGrid() {
        for (int row = 0; row < N; row++) {
            for (int col = 0; col < N; col++)
                System.out.print(this.grid[row][col]+" ");
            System.out.println();
        }
    }
    void printSolved(){
        for (int row = 0; row < N; row++) {
            for (int col = 0; col < N; col++)
                System.out.print(this.solvedGrid[row][col]+" ");
            System.out.println();
        }
    }

    void saveHints(){
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                hints[i][j] = 0; //inicjacja aby nie bylo smieci
                if (this.solvedGrid[i][j] != 0) {
                    System.out.println("Podpowiedz dla " + i + ' ' + j + " to :" + this.solvedGrid[i][j]);
                    hints[i][j] = this.solvedGrid[i][j];
                }
            }
        }
    }

    boolean solveOpened(Cell cur) {

        // tak jak wyzyej, jesli null to rozwiazane
        if (cur == null) {
            return true;
        }

        // jesli grid[current] ma cos innego niz zero (czyli jest wypelniona) to nic nie mam tu do roboty
        if (solvedGrid[cur.row][cur.col] != 0) {
            //Kwintesencja algorytmu, jezeli jest wypelniona to lec po nowa komorke
            return solveOpened(getNextCell(cur));
        }


        // jesli grid[current] nie ma nic to sprawdz czy w komore current mozna zapakowac i
        for (int i = 1; i <= 9; i++) {
            // sprawdz czy jest ok, jesli tak to nizej wypelnij to wartoscia z i
            boolean valid = isValidOpen(cur, i);

            if (!valid) // i not valid for this cell, try other values
                continue;

            // przypisanie
            solvedGrid[cur.row][cur.col] = i;

            //---------------------------------------------------------------tutaj tescik jak fajnie wypelnia
            for (int row = 0; row < N; row++) {
                for (int col = 0; col < N; col++)
                    System.out.print(grid[row][col]+" ");
                System.out.println();
            }
            System.out.println("\n\n\n---------------------------------------------------------------\n\n\n");
            //---------------------------------------------------------------


            // lec z nastepna komorka
            boolean solved = solveOpened(getNextCell(cur));
            // jesli jest wypelnione to true
            if (solved)
                return true;
            else // jesli nie to wpisz zero i sie cofamy
                solvedGrid[cur.row][cur.col] = 0;
            //---------------------------------------------------------------tescik
            for (int row = 0; row < N; row++) {
                for (int col = 0; col < N; col++)
                    System.out.print(grid[row][col]+" ");
                System.out.println();
            }
            System.out.println("\n\n\n---------------------------------------------------------------\n\n\n");
            //---------------------------------------------------------------
            // continue z innymi wartosciami
        }


        // Jezeli jest cos nie poprawne to zwroc false
        System.out.println("Hujowo, nie mozna");
        return false;
    }
    boolean isValidOpen(Cell cell, int value) {

        if (solvedGrid[cell.row][cell.col] != 0) {
            System.out.println("Pole ma juz wartosc rozna od zera - czyli nie jest puste");
        }


        for (int c = 0; c < 9; c++) {
            if (solvedGrid[cell.row][c] == value || solvedGrid[c][cell.col] == value)
                return false;
        }

        // tak jak rozmawialismy na zajeciach, wyznaczanie kwadratu, a wlasciwie jego wierzcholkow (wspolrzednych).
        int x1 = 3 * (cell.row / 3);
        int y1 = 3 * (cell.col / 3);
        //stworzenie kolejnego wierzchilka
        int x2 = x1 + 2;
        int y2 = y1 + 2;

        //iteracja kwadratu
        for (int x = x1; x <= x2; x++) {
            for (int y = y1; y <= y2; y++) {
                if (solvedGrid[x][y] == value) {
                    return false;
                }
            }
        }
        // jezeli liczby nie ma w pionie, poziomie i boxie, return true
        return true;
    }



}