package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class TimerHandler implements EventHandler<ActionEvent> {
    private Label label ;
    private int seconds = 0;
    private int minutes = 0;
    public TimerHandler(Label label) {
        this.label = label;
    }



    @Override
    public void handle(ActionEvent arg0) {
        seconds++;
        if (seconds == 60) {
            seconds = 0;
            minutes++;
        }
        if (minutes==0 && seconds==0) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("End of time");
            alert.setHeaderText("Sorry");
            alert.setContentText("Please try again");
            alert.show();

        }
        label.setText(String.format("%02d:%02d", minutes, seconds));

    }
    public void reset(){
        seconds = 0;
        minutes = 0;
        label.setText("00:00");
    }

}
