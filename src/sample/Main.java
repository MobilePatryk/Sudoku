package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.io.*;
import java.util.*;


public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) {
        GridPane table = new GridPane();

        Label label = new Label("00:00");
        Button bPrint = new Button("Print output");
        Button bSave = new Button("Save Game");
        Button bOpen = new Button("Open");
        Button bStart = new Button("Start");
        Button bHint = new Button("Hint");
        Button bCheck = new Button("Check");

        label.setStyle("-fx-border-color: BLUE;");
        label.setFont(new Font(34));



        TimerHandler timerHandler = new TimerHandler(label);
        KeyFrame keyFrame = new KeyFrame(Duration.seconds(1), timerHandler);
        Timeline timer = new Timeline(keyFrame);
        timer.setCycleCount (300);

        VBox vBox = new VBox();

        vBox.getChildren().addAll(table, bPrint, bSave,bOpen,bStart,bHint,bCheck,label);
        Scene scene = new Scene(vBox, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();

        SudokuSolver ss = new SudokuSolver();
        SudokuGrid userTable = new SudokuGrid(ss,table);

        userTable.binder();

        bPrint.setOnAction(event -> {
                    System.out.println("\n\n\n A o to tablica użytkownika \n\n\n\n");
                    for (int row = 0; row < 9; row++) {
                        for (int col = 0; col < 9; col++)
                            System.out.print(userTable.sv.grid[row][col] + " ");
                        System.out.println();
                    }
                });


        bSave.setOnAction(e -> {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Save file");
                File savedFile = fileChooser.showSaveDialog(primaryStage);
                if(savedFile != null){
                    userTable.save(savedFile);
                }
        });

        bOpen.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open File");
            File openFile = fileChooser.showOpenDialog(primaryStage);
            if(openFile != null){
                userTable.open(openFile);
                timer.play();
            }
        });

        bStart.setOnAction(e -> {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("New Game");
            alert.setHeaderText("Chose the difficulty");

            ButtonType buttonTypeOne = new ButtonType("Easy");
            ButtonType buttonTypeTwo = new ButtonType("Medium");
            ButtonType buttonTypeThree = new ButtonType("Hard");
            ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree, buttonTypeCancel);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne){
                ss.generate(8,10);
                userTable.display();
            } else if (result.get() == buttonTypeTwo) {
                ss.generate(8,30);
                userTable.display();
                // ... user chose "Two"
            } else if (result.get() == buttonTypeThree) {
                ss.generate(8,65);
                userTable.display();
                // ... user chose "Three"
            } else {
                // ... user chose CANCEL or closed the dialog
            }
            timer.play();
        });

       bHint.setOnAction(e->{
           boolean flag = true;
           for (int i = 0; i < 9; i++) {
               for (int j = 0; j < 9; j++){
                   if(userTable.sv.grid[i][j] == 0 && flag){
                       TextField t = (TextField)table.getChildren().get(i*9+j);
                      // userTable.clearGreen();
                       t.setStyle("-fx-control-inner-background: GREEN");
                       t.setText(String.valueOf(userTable.sv.hints[i][j]));
                       userTable.sv.grid[i][j] = userTable.sv.hints[i][j];
                       flag = false;
                   }
               }

           }
        });

       bCheck.setOnAction(e->{
           if(userTable.checkSolution()){
               timer.stop();
           }
       });



    }
}
